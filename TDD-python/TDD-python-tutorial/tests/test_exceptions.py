import pytest

from code_examples.list_func import select_item_from_list, find_item_position_in_list
from code_examples.division_func import division


def test_division_by_zero():
    with pytest.raises(ZeroDivisionError):
        division(1, 0)


def test_select_item_from_list():
    with pytest.raises(IndexError):
        select_item_from_list([1, 2, 3], 10)

    with pytest.raises(TypeError):
        select_item_from_list('list A', 'index')


def test_find_item_position_in_list():
    with pytest.raises(ValueError):
        find_item_position_in_list([], 1)
