import pytest

from code_examples.division_func import division


def test_division_by_two():
    assert division(4, 2) == 2  # SHOULD PASS
    # assert division(9, 2) == 100  # SHOULD FAIL
    assert division(1000, 2) == 500  # SHOULD PASS


def test_division_by_three():
    assert division(9, 3) == 3  # SHOULD PASS
    assert division(30, 3) == 10  # SHOULD PASS
    assert division(9999, 3) == 3333  # SHOULD PASS


def test_division_by_zero():
    with pytest.raises(ZeroDivisionError):
        division(1, 0)
