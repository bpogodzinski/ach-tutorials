#include <gtest/gtest.h>
#include "simple_functions.h"


TEST(Division, ZeroInput){
    EXPECT_LE(division(1,0),0) << "Should be negative number error.";
    EXPECT_LE(division(0,0),0);
    EXPECT_EQ(division(0,1),0);
}

TEST(Division, PositiveInput){
    // Expect to be equal:
    EXPECT_EQ(division(2,1), 2);
    EXPECT_EQ(division(2,2),1);
    EXPECT_EQ(division(1,2),0);

    // Assert to be equal:
    ASSERT_EQ(division(0,2),0);
    ASSERT_EQ(division(3,2),1);
    ASSERT_EQ(division(14,5),2);
}