#include <gtest/gtest.h>
#include "velocity.h"


TEST(ExampleVelocity, ZeroInput){
    EXPECT_LE(velocity(1,0),0) << "Should be negative number error.";
    EXPECT_LE(velocity(0,0),0);
    EXPECT_EQ(velocity(0,1),0);
}

TEST(ExampleVelocity, PositiveInput){
    // Expect to be equal:
    EXPECT_EQ(velocity(2,1), 2);
    EXPECT_EQ(velocity(2,2),1);
    EXPECT_EQ(velocity(1,2),0);

    // Assert to be equal:
    ASSERT_EQ(velocity(0,2),0);
    ASSERT_EQ(velocity(3,2),1);
    ASSERT_EQ(velocity(14,5),2);
}