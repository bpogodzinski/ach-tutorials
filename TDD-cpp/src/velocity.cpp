#include "velocity.h"

int velocity(int distance_m, int time_s){
    if(time_s <= 0){
        return -999999999;
    }
    return int(distance_m / time_s);
}