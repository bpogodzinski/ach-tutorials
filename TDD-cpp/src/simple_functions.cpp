#include "simple_functions.h"


int division(int dividend, int divisor){
    if(divisor == 0){
        return 0;
    }
    return int(dividend / divisor);
}