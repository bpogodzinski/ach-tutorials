cmake_minimum_required(VERSION 3.16)
project(TDD-cpp)

set(CMAKE_CXX_STANDARD 11)

include_directories(src)
add_subdirectory(src)

enable_testing()
add_subdirectory(tst)



